var Storage = artifacts.require("Storage");
var SimpleAuction = artifacts.require("SimpleAuction");

module.exports = function(deployer) {
    deployer.deploy(Storage, [20]);
    deployer.deploy(SimpleAuction, 10, "dsadas", "0x085BB11B95DfAc0e01377b3Fc183708479687C2d")
};
