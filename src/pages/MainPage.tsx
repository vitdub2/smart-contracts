import {useContext} from "react";
import {EthereumContext} from "../contexts/EthereumContext";
//@ts-ignore
import contractCode from '../truffle/build/contracts/Storage.json';
import {Button, Col} from "antd";
import Typography from "antd/lib/typography";

const { Title } = Typography;

export const MainPage = () => {
    const { account, balance } = useContext(EthereumContext);

    return (
        <Col>
            <Title level={2}>
                {account}
            </Title>
            <Title level={3}>
                {balance} ETH
            </Title>
        </Col>
    )
}
