import { Outlet } from "react-router-dom";
import TransactionsContextProvider from "../../contexts/TransactionsContext";
import ContractsContextProvider from "../../contexts/ContractsContext";

const AccountPage = () => {
    return (
        <TransactionsContextProvider>
            <ContractsContextProvider>
                <Outlet/>
            </ContractsContextProvider>
        </TransactionsContextProvider>
    )
}

export default AccountPage;
