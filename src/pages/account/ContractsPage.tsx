import {useContext} from "react";
import {ContractsContext} from "../../contexts/ContractsContext";
import {Col, List, Row, Typography} from "antd";
import {NavLink} from "react-router-dom";

const { Title } = Typography;

const ContractsPage = () => {
    const { accountContractList } = useContext(ContractsContext);
    return (
        <Col>
            <Title level={2}>
                Contracts
            </Title>
            <List
                size="large"
                header={null}
                footer={null}
                bordered
                dataSource={accountContractList}
                renderItem={(contract, index) => (
                    <List.Item>
                        <NavLink to={`/contracts/${contract.address}`}>
                            {index + 1}. {contract.address}
                        </NavLink>
                    </List.Item>
                )}
            />
        </Col>
    );
}

export default ContractsPage;
