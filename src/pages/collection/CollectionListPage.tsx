import {useContext, useEffect, useState} from "react";
import {EthereumContext} from "../../contexts/EthereumContext";
import {FirebaseContext} from "../../contexts/FirebaseContext";

import { ref as databaseRef, get, child } from "firebase/database";
import {List, Row} from "antd";
import {NavLink} from "react-router-dom";

const CollectionListPage = () => {
    const { account } = useContext(EthereumContext);
    const { database } = useContext(FirebaseContext);

    const [images, setImages] = useState<string[]>([])

    useEffect(() => {
        const dbRef = databaseRef(database);
        get(child(dbRef, `collection/${account.toLowerCase()}`)).then((snapshot) => {
            if (snapshot.exists()) {
                const value = snapshot.val();
                setImages(Object.keys(value));
            }
        })
    }, [database, account]);

    return (
        <Row>
            <List
                bordered
                dataSource={images}
                renderItem={(image) => (
                    <List.Item>
                        <NavLink to={`/collection/${image}`}>
                            {image}
                        </NavLink>
                    </List.Item>
                )}
            />
        </Row>
    )
}

export default CollectionListPage;
