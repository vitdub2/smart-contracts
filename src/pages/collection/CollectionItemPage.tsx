import {getDownloadURL, ref as storageRef} from "firebase/storage";
import {ref as databaseRef, get} from "firebase/database";
import React, {useContext, useEffect, useMemo, useState} from "react";
import {FirebaseContext} from "../../contexts/FirebaseContext";
import {NavLink, useParams} from "react-router-dom";
import {Col, Row} from "antd";
import {ContractsContext} from "../../contexts/ContractsContext";
import {EthereumContext} from "../../contexts/EthereumContext";
import {Loading3QuartersOutlined} from "@ant-design/icons";


const CollectionItemPage = () => {
    const { storage, database } = useContext(FirebaseContext);
    const { getContract } = useContext(ContractsContext);
    const { account } = useContext(EthereumContext);

    const [itemUrl, setItemUrl] = useState<string>();
    const [address, setAddress] = useState<string>();
    const [bidder, setBidder] = useState<string>();

    const params = useParams();

    const id = params.id;

    useEffect(() => {
        const item = `images/${id}`;
        getDownloadURL(storageRef(storage, item)).then(setItemUrl);
    }, [storage, id]);

    useEffect(() => {
        get(databaseRef(database, `/images/${id}`)).then((snapshot) => {
            if (snapshot.exists()) {
                setAddress(snapshot.val());
            }
        })
    }, [database, id]);

    const contract = getContract(address);

    useEffect(() => {
        if (contract && address) {
            contract.methods.highestBidder().call().then(setBidder);
        }
    }, [contract, address]);

    const hasAccess = account === bidder?.toLowerCase();

    if (!bidder) {
        return (
            <Loading3QuartersOutlined style={{
                color: "#4096ff",
                fontSize: "50px",
                marginTop: "20px",
            }} spin/>
        )
    }

    if (!hasAccess) {
        return (
            <Col>
                <Row>
                    You are not allowed to see this content
                </Row>
            </Col>
        )
    }

    return (
        <Col>
            <Row>
                <NavLink to={`/contracts/${address}`}>
                    Contract link
                </NavLink>
            </Row>
            <Row>
                <img src={itemUrl}/>
            </Row>
        </Col>
    );
}

export default CollectionItemPage;
