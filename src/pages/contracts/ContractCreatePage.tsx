import {useContext, useState} from "react";
import {EthereumContext} from "../../contexts/EthereumContext";
import { UploadOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import {Button, Col, Form, InputNumber, Upload} from 'antd';
import {RcFile} from "antd/lib/upload";
import {FirebaseContext} from "../../contexts/FirebaseContext";
import { ref as storageRef, uploadBytes } from "firebase/storage";
import { ref as databaseRef, set } from "firebase/database";
import { v4 as uuidv4 } from 'uuid';
import contractCode from '../../truffle/build/contracts/SimpleAuction.json';
import {useNavigate} from "react-router-dom";
import {ContractsContext} from "../../contexts/ContractsContext";

const CreateContractPage = () => {
    const { web3, account } = useContext(EthereumContext);
    const { getContract } = useContext(ContractsContext);
    const { storage, database } = useContext(FirebaseContext);

    const navigate = useNavigate();

    const [file, setFile] = useState<RcFile | null>(null);

    const [isCreating, setCreating] = useState(false);

    const handleCreateContract = (values: any) => {
        if (web3) {
            setCreating(true);
            const uuid = uuidv4();
            const storRef = storageRef(storage, `images/${uuid}`);
            uploadBytes(storRef, file as File).then((fileInfo) => {
                const path = fileInfo.metadata.fullPath;

                const contract = getContract();
                if (contract) {
                    contract.deploy({
                        data: contractCode.bytecode,
                        arguments: [values.time, path, account]
                    }).send({
                        from: account,
                        gas: 1500000,
                    }).then((newContractInstance) => {
                        const address = newContractInstance.options.address;
                        const contractRef = databaseRef(database, `images/${uuid}`);
                        set(contractRef, address).then(() => {
                            navigate(`/contracts/${address}`);
                            setCreating(false);
                        })
                    });
                }
            });
        }
    }

    const props: UploadProps = {
        beforeUpload: (info) => {
            setFile(info);
            return false;
        },
    };
    return (
        <Col>
            <Form onFinish={handleCreateContract}>
                <Form.Item
                    name="time"
                    label="Auction time"
                >
                    <InputNumber min={0}/>
                </Form.Item>
                <Form.Item
                    name="file"
                    label="Auction file"
                >
                    <Upload {...props}>
                        <Button icon={<UploadOutlined />}>
                            Upload
                        </Button>
                    </Upload>
                </Form.Item>
                <Button htmlType="submit" loading={isCreating} onClick={handleCreateContract}>
                    Create contract
                </Button>
            </Form>
        </Col>
    )
}

export default CreateContractPage;
