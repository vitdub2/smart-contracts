import {useContext} from "react";
import {ContractsContext} from "../../contexts/ContractsContext";
import {Col, List, Row} from "antd";
import {NavLink} from "react-router-dom";

const ContractListPage = () => {
    const { contractList } = useContext(ContractsContext);

    return (
        <Col>
            <List
                bordered
                dataSource={contractList.filter((c) => !c.ended)}
                renderItem={(item) => (
                    <List.Item>
                        <NavLink to={`/contracts/${item.address}`}>
                            {item.address}
                        </NavLink>
                    </List.Item>
                )}
            />
        </Col>
    )
}

export default ContractListPage;
