import {useParams} from "react-router-dom";
import contractCode from "../../truffle/build/contracts/SimpleAuction.json";
import {useContext, useEffect, useMemo, useState} from "react";
import {EthereumContext} from "../../contexts/EthereumContext";
import {Button, Col, Collapse, Form, InputNumber, message, Row, Typography} from "antd";
import {FirebaseContext} from "../../contexts/FirebaseContext";
import { ref, getDownloadURL } from "firebase/storage";
import {ref as databaseRef} from "@firebase/database";
import {get, set} from "firebase/database";

const { Title, Text } = Typography;
const { Panel } = Collapse;

const ContractItemPage = () => {
    const params = useParams();

    const { web3, account } = useContext(EthereumContext);

    const { database, storage } = useContext(FirebaseContext);

    const { id } = params;

    const contract = useMemo(() => web3 && new web3.eth.Contract(contractCode.abi as any, id), [web3]);

    const methods = contract?.methods;

    const [item, setItem] = useState<string>(``);
    const [itemUrl, setItemUrl] = useState<string>(``);
    const [beneficiary, setBeneficiary] = useState<string>(``);
    const [highestBid, setHighestBid] = useState<number>(0);
    const [highestBidder, setHighestBidder] = useState<string>(``);
    const [auctionEndTime, setAuctionEndTime] = useState<Date>(new Date());
    const [ended, setEnded] = useState<boolean>(true);

    const fetchData = () => {
        if (methods) {
            methods.getItem().call().then(setItem);
            methods.getBeneficiary().call().then(setBeneficiary);
            methods.getHighestBid().call().then((bid: string) => setHighestBid(+bid));
            methods.getHighestBidder().call().then(setHighestBidder);
            methods.getAuctionEndTime().call().then((time: string) => setAuctionEndTime(new Date(+time * 1000)));
            methods.ended().call().then(setEnded);
        }
    }

    useEffect(() => {
        fetchData();
    }, [contract]);

    useEffect(() => {
        if (item && !itemUrl) {
            getDownloadURL(ref(storage, item)).then(setItemUrl);
        }
    }, [item, itemUrl]);

    const [now, setNow] = useState(new Date().getTime());

    useEffect(() => {
        const interval = setInterval(() => {
            setNow(new Date().getTime());
        }, 1000);

        return () => {
            clearInterval(interval);
        }
    }, [])

    const diff = auctionEndTime.getTime() - now;

    const seconds = Math.floor(diff / 1000);

    const minutes = Math.floor(seconds / 60);

    const auctionIsGoing = seconds > 0;

    const isBeneficiary = beneficiary.toString().toLowerCase() === account.toLowerCase();

    const [makingBid, setMakingBid] = useState<boolean>(false);

    const handleBid = (values: any) => {
        setMakingBid(true);
        methods.bid()
            .send({from: account, value: values.amount})
            .then(() => fetchData())
            .finally(() => {
                setMakingBid(false);
            });
    }

    const handleEnd = () => {
        const accountRef = databaseRef(database, `collection/${highestBidder.toLowerCase()}`);
        get(accountRef).then(async (accountInfo) => {
            if (!accountInfo.exists()) {
                await set(accountRef, {});
                return await get(accountRef)
            }
            return accountInfo;
        }).then((accountInfo) => {
            const value = accountInfo.val();

            const uuid = item.split("images/")[1];
            set(accountRef, {...value, [uuid]: true }).then(() => {
                methods.auctionEnd().send({from: account}).then(() => fetchData());
            })
        })
    }

    return (
        <Col>
            {auctionIsGoing ? (
                <Row>
                    <Title level={3}>
                        {minutes}:{(seconds % 60).toString().padStart(2, "0")}
                    </Title>
                </Row>
            ) : (
                <Row>
                    <Title level={3}>
                        Auction has ended
                    </Title>
                </Row>
            )}
            <Col>
                <Collapse>
                    <Panel key={itemUrl} header="Image">
                        <img src={itemUrl}/>
                    </Panel>
                </Collapse>
                {!!highestBid && (
                    <Col>
                        <Row>
                            <Text>
                                Highest bid: {highestBid}
                            </Text>
                        </Row>
                        <Row>
                            <Text>
                                Highest bidder: {highestBidder}
                            </Text>
                        </Row>
                    </Col>
                )}
                {auctionIsGoing && !isBeneficiary && (
                    <Row style={{
                        marginTop: "10px",
                    }}>
                        <Form onFinish={handleBid}>
                            <Form.Item
                                name="amount"
                                help="Field is required"
                                label="Bid"
                            >
                                <InputNumber min={highestBid}/>
                            </Form.Item>
                            <Form.Item>
                                <Button loading={makingBid} htmlType="submit" type="primary">
                                    Make a bid
                                </Button>
                            </Form.Item>
                        </Form>
                    </Row>
                )}
                {!auctionIsGoing && !ended && isBeneficiary && (
                    <Row style={{
                        marginTop: "10px",
                    }}>
                        <Button onClick={handleEnd} type="primary">
                            End auction
                        </Button>
                    </Row>
                )}
            </Col>
        </Col>
    )
}

export default ContractItemPage;
