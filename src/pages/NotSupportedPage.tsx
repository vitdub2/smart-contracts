import {Col, Row, Typography} from "antd";
import MetamaskLogo from '../assets/MetaMask.png';

const { Text, Link } = Typography;

const NotSupportedPage = () => {
    return (
        <Col>
            <Row justify="center">
                <img src={MetamaskLogo} height={200} alt="Metamask logo"/>
            </Row>
            <Row justify="center">
                <Text>
                    We`ve detected that Metamask not installed at your browser
                </Text>
            </Row>
            <Row justify="center">
                <Text>
                    To run your app, you need to install <Link href="https://metamask.io/download/" target="_blank">Metamask extension </Link>
                </Text>
            </Row>
        </Col>
    )
}

export default NotSupportedPage;
