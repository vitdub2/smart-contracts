import { Route, Routes } from "react-router-dom";
import {MainPage} from "./pages/MainPage";
import AppLayout from "./layout/AppLayout";
import AccountPage from "./pages/account/AccountPage";
import TransactionsPage from "./pages/account/TransactionsPage";
import ContractsPage from "./pages/account/ContractsPage";
import CollectionListPage from "./pages/collection/CollectionListPage";
import CollectionItemPage from "./pages/collection/CollectionItemPage";
import ContractListPage from "./pages/contracts/ContractListPage";
import ContractCreatePage from "./pages/contracts/ContractCreatePage";
import ContractItemPage from "./pages/contracts/ContractItemPage";

export const Router = () => {
    return (
        <Routes>
            <Route path="/" element={<AppLayout/>}>
                <Route index element={<MainPage/>}/>
                <Route path="account" element={<AccountPage/>}>
                    <Route path="transactions" element={<TransactionsPage/>}/>
                    <Route path="contracts" element={<ContractsPage/>}/>
                </Route>
                <Route path="collection">
                    <Route index element={<CollectionListPage/>}/>
                    <Route path=":id" element={<CollectionItemPage/>}/>
                </Route>
                <Route path="contracts">
                    <Route index element={<ContractListPage/>}/>
                    <Route path="create" element={<ContractCreatePage/>}/>
                    <Route path=":id" element={<ContractItemPage/>}/>
                </Route>
            </Route>
        </Routes>
    )
}
