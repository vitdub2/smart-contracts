import React, { useState } from 'react';
import {
    UserOutlined,
    BarsOutlined,
    FolderOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Layout, Menu, theme } from 'antd';
import {Outlet, useLocation, useNavigate} from 'react-router-dom';

const { Header, Content, Footer, Sider } = Layout;

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
    } as MenuItem;
}

const items: MenuItem[] = [
    getItem('Account', '', <UserOutlined />, [
        getItem('My account', '/'),
        getItem('My transactions', '/account/transactions'),
        getItem('My contracts', '/account/contracts'),
    ]),
    getItem('Collection', '/collection', <FolderOutlined />),
    getItem('Contracts', '', <BarsOutlined />, [
        getItem('All contracts', '/contracts'),
        getItem('Create contract', '/contracts/create'),
    ]),
];

const AppLayout: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false);
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    const navigate = useNavigate();

    const onClick: MenuProps['onClick'] = (e) => {
        navigate(e.key);
    };

    const location = useLocation();

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div style={{ height: 32, margin: 16, background: 'rgba(255, 255, 255, 0.2)' }} />
                <Menu theme="dark" defaultSelectedKeys={[location.pathname]} onClick={onClick} mode="inline" items={items} />
            </Sider>
            <Layout className="site-layout">
                <Header style={{ padding: 0, background: colorBgContainer }} />
                <Content style={{ margin: '0 16px' }}>
                    <Outlet/>
                </Content>
            </Layout>
        </Layout>
    );
};

export default AppLayout;
