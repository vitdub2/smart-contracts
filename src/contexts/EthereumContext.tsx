import {createContext, useCallback, useEffect, useMemo, useState} from "react";
import Web3 from "web3";

declare global {
    interface Window {
        readonly ethereum: {
            readonly isConnected: () => boolean;
            readonly request: (params: any) => Promise<any>;
            readonly on: (event: string, handler: (info?: any) => void) => void;
        }
    }
}

type EthereumContextType = {
    readonly isConnected: boolean;
    readonly isAuth: boolean;
    readonly account: string;
    readonly balance: number;

    readonly web3: Web3 | null;

    readonly gasPrice: string;

    readonly connect: () => void;
}


export const EthereumContext = createContext<EthereumContextType>({
    isAuth: false,
    isConnected: false,
    account: ``,
    balance: 0,
    web3: null,
    gasPrice: ``,
    connect: () => {}
});
EthereumContext.displayName = "EthereumContext";

const EthereumContextProvider = ({ children }: any) => {
    const [isConnected, setConnected] = useState<boolean>(window.ethereum.isConnected());
    const [isAuth, setAuth] = useState<boolean>(false);
    const [account, setAccount] = useState<string>(``);
    const [balance, setBalance] = useState<number>(0);

    const web3 = useMemo(() => {
        if (isConnected) {
            return new Web3("http://localhost:7545")
        } else {
            return null;
        }
    }, [isConnected]);

    const [gasPrice, setGasPrice] = useState<string>(`0`);

    const authorize = (accounts: any[]) => {
        if (accounts.length > 0) {
            setAccount(accounts[0]);
            setAuth(true);
        }
    }

    window.ethereum.on(`connect`, () => {
        setConnected(true);
    })

    const getBalance = useCallback(() => {
        if (web3) {
            window.ethereum
                .request({ method: 'eth_getBalance', params: [account, 'latest'] })
                .then((resp) => {
                    setBalance(+web3.utils.fromWei(resp));
                })
        }
    }, [account, web3]);

    useEffect(() => {
        if (account) {
            getBalance();
        }
    }, [account]);

    const getAccounts = () => {
        window.ethereum.request({ method: 'eth_accounts' }).then(authorize);
    }

    const connect = () => {
        window.ethereum.request({ method: 'eth_requestAccounts' }).then(authorize);
    }

    window.ethereum.on('accountsChanged', (info) => {
        window.location.reload();
    })

    const disconnect = () => {
        setConnected(false);
        setAccount(``);
        setBalance(0);
    }

    const getGasPrice = () => {
        web3 && web3.eth.getGasPrice().then((price) => {
            setGasPrice(price);
        })
    }

    useEffect(() => {
        if (!isAuth && isConnected) {
            getAccounts();
        }
    }, [isAuth, isConnected]);

    useEffect(() => {
        if (isAuth && web3) {
            getGasPrice();
        }
    }, [isAuth, web3]);

    const value = {
        isConnected,
        isAuth,
        account,
        balance,
        connect,
        web3,
        gasPrice,
    };

    return (
        <EthereumContext.Provider value={value}>
            {children}
        </EthereumContext.Provider>
    )
}

export default EthereumContextProvider;
