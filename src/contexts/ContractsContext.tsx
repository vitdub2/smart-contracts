import {createContext, useContext, useEffect, useMemo, useState} from "react";
import {TransactionsContext} from "./TransactionsContext";
import {TransactionReceipt} from "web3-core";
import contractCode from "../truffle/build/contracts/SimpleAuction.json";
import {EthereumContext} from "./EthereumContext";
import {Contract as WebContract} from 'web3-eth-contract';

export type Contract = {
    address: string;
    ended: boolean,
}

type ContractsContextType = {
    contractList: Contract[];
    accountContractList: Contract[];
    getContract: (address?: string) => WebContract | null;
}

export const ContractsContext = createContext<ContractsContextType>({
    contractList: [],
    accountContractList: [],
    getContract: () => null,
});
ContractsContext.displayName = "ContractsContext";

const ContractsContextProvider = ({ children }: any) => {
    const { web3 } = useContext(EthereumContext);
    const { transactions, accountTransactions } = useContext(TransactionsContext);

    const getContract = (address?: string) => {
        if (web3) {
            if (address) {
                return new web3.eth.Contract(contractCode.abi as any, address);
            }
            return new web3.eth.Contract(contractCode.abi as any);
        }

        return null;
    }

    const getContractTransactions = (receipts: TransactionReceipt[]) => receipts.filter((transaction) => !transaction.to);

    const getContractList = async (receipts: TransactionReceipt[]) => {
        const contracts = receipts.map((transaction) => transaction.contractAddress || ``).map(async (address) => ({
            address,
            ended: await getContract(address)?.methods.ended().call().catch(() => true),
        }));

        return await Promise.all(contracts);
    }

    const [contractList, setContractList] = useState<Contract[]>([]);
    const [accountContractList, setAccountContractList] = useState<Contract[]>([]);

    const contractTransactions = useMemo(() => getContractTransactions(transactions), [transactions]);

    const accountContractTransactions = useMemo(() => getContractTransactions(accountTransactions), [accountTransactions]);

    useEffect(() => {
        //@ts-ignore
        getContractList(contractTransactions).then(setContractList);
    }, [contractTransactions]);

    useEffect(() => {
        //@ts-ignore
        getContractList(accountContractTransactions).then(setAccountContractList);
    }, [accountContractTransactions]);

    const value = {
        contractList,
        accountContractList,
        getContract,
    }

    return (
        <ContractsContext.Provider value={value}>
            {children}
        </ContractsContext.Provider>
    )
}

export default ContractsContextProvider;
