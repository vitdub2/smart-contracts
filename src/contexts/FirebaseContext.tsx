import {createContext} from "react";
import { initializeApp } from "firebase/app";
import {FirebaseStorage, getStorage } from "firebase/storage";
import { getDatabase, Database } from "firebase/database";

type FirebaseContextType = {
    storage: FirebaseStorage;
    database: Database;
}

const firebaseConfig = {
    apiKey: "AIzaSyC0UZF_QU2geOjwhEysJ-CirVch5Ug1Me8",
    authDomain: "smart-contracts-17b16.firebaseapp.com",
    databaseURL: "https://smart-contracts-17b16-default-rtdb.firebaseio.com",
    projectId: "smart-contracts-17b16",
    storageBucket: "smart-contracts-17b16.appspot.com",
    messagingSenderId: "582968865846",
    appId: "1:582968865846:web:a767dc8b9a91c8a1765497"
};

export const FirebaseContext = createContext<FirebaseContextType>({
    storage: getStorage(initializeApp(firebaseConfig)),
    database: getDatabase(initializeApp(firebaseConfig)),
})
FirebaseContext.displayName = "FirebaseContext";

const FirebaseContextProvider = ({ children }: any) => {
    const app = initializeApp(firebaseConfig);

    const storage = getStorage(app);

    const database = getDatabase(app);

    const value: FirebaseContextType = {
        storage,
        database,
    }

    return (
        <FirebaseContext.Provider value={value}>
            {children}
        </FirebaseContext.Provider>
    )
}

export default FirebaseContextProvider;
